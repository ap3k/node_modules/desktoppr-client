# Desktoppr API client (deprecated since desktoppr.co status is unknown)

NPM package: https://www.npmjs.com/package/desktoppr-client

Git repo: https://gitlab.com/ap3k/node_modules/desktoppr-client

> Desktoppr: https://www.desktoppr.co/
>
> Desktoppr API doc: https://www.desktoppr.co/api

## Installation

With NPM

```
npm install desktoppr-client --save
```

With Yarn

```
yarn add desktoppr-client
```

## Usage
> Read the Source Luke!

### User
```js
// Require it in
const { User } = require('desktoppr-client')

// Creates an instance of User.
const user = new User('test')

// Get user information based on a users username
user.get()

// Get paginated wallpapers from user collection
user.wallpapers()

// Grab a random wallpaper from user collection
// Turn 'safe' = false to get NSFW images
user.random(safe = true)

// Get paginated list of user liked wallpapers
user.liked()

// Get user followers
user.followers()

// Get users that given user follows
user.following()

// Follow given user
// 'user' is user to follow
// 'password' is currently initialized user password
user.follow(user = '', password = '')

// Unfollow given user
// 'user' is user to unfollow
// 'password' is currently initialized user password
user.unfollow(user = '', password = '')
```

### Wallpapers

```js
// Require it in
const { Wallpapers } = require('desktoppr-client')

// Get all wallpapers in database, Paginated by 10
// 'page' is current pagination page
Wallpapers.all(page = 0)

// Get random SFW image from database
Wallpapers.random()
```
