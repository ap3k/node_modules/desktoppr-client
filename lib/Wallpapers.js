const axios = require('./axios')

module.exports = class Wallpapers {
  /**
   * Get all wallpapers in the database that have been marked as safe
   *
   * @param {number} [page=0] Pagination page
   * @returns {Promise}
   */
  static all(page = 0) {
    return axios.get(`wallpapers?page=${page}`)
      .then(({ data }) => {
        return data
      })
      .catch((e) => {
        console.error(e)
        return e
      })
  }

  /**
   * Grab a random SFW wallpaper from entire database
   *
   * @returns {Promise}
   */
  static random() {
    return axios.get('wallpapers/random')
      .then(({ data }) => {
        return data
      })
      .catch((e) => {
        console.error(e)
        return
      })
  }
}
