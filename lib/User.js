const axios = require('./axios')

module.exports = class User {

  /**
   * Creates an instance of User.
   *
   *  @param {string} [username='']
   */
  constructor(username = '') {
    this.username = username
  }

  /**
   * Get user information based on a users username
   *
   * @returns {Promise}
   */
  get() {
    return axios.get(`users/${this.username}`)
      .then(({ data }) => {
        return data
      })
      .catch((e) => {
        console.error(e)
        return e
      })
  }

  /**
   * Get paginated wallpapers from user collection
   *
   * @param {boolean} [safe=true]
   * @returns {Promise}
   */
  wallpapers(safe = true) {
    var url = `users/${this.username}/wallpapers`

    if (!safe) {
      url += '?safe_filter=all'
    }
    return axios.get(url)
      .then(({ data }) => {
        return data
      })
      .catch((e) => {
        console.log(e)
        return e
      })
  }

  /**
   * Grab a random wallpaper from user collection
   *
   * @returns {Promise}
   */
  random() {
    return axios.get(`users/${this.username}/wallpapers/random`)
      .then(({ data }) => {
        return data
      })
      .catch((e) => {
        console.error(e)
        return
      })
  }

  /**
   * Get paginated list of user liked wallpapers
   *
   * @returns {Promise}
   */
  liked() {
    return axios.get(`users/${this.username}/likes`)
      .then(({ data }) => {
        return data
      })
      .catch((e) => {
        console.error(e)
        return
      })
  }

  /**
   * Get user followers
   *
   * @returns {Promise}
   */
  followers() {
    return axios.get(`users/${this.username}/followers`)
      .then(({ data }) => {
        return data
      })
      .catch((e) => {
        console.error(e)
        return
      })
  }

  /**
   * Get users that given user follows
   *
   * @returns {Promise}
   */
  following() {
    return axios.get(`users/${this.username}/following`)
      .then(({ data }) => {
        return data
      })
      .catch((e) => {
        console.error(e)
        return
      })
  }

  /**
   * Follow given user
   *
   * @param {string} [user=''] User to follow
   * @param {string} [password=''] Current user password
   *
   * @returns {Promise}
   */
  follow(user = '', password = '') {
    return axios.post(`users/${user}/follow`, {
      auth: {
        username: this.username,
        password: password
      }
    })
      .then(({ data }) => {
        return data
      })
      .catch((e) => {
        console.error(e)
        return
      })
  }

  /**
   * Unfollow given user
   *
   * @param {string} [user=''] User to unfollow
   * @param {string} [password=''] Current user password
   *
   * @returns {Promise}
   */
  unfollow(user = '', password = '') {
    return axios.delete(`users/${user}/follow`)
      .then(({ data }) => {
        return data
      })
      .catch((e) => {
        console.error(e)
        return
      })
  }
}
